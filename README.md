


## Project Overview
Our project is a mobile application (and a related backend API) that helps users avoid the frustration of having to color coordinate their outfits.

Users tell the application which specific pieces of clothing they want to wear and the app provides suggestions for what would make a good match based on color and style. Matches can be made with clothes that the user already owns as well as clothes that can be purchased from popular online retailers. Users can save and share their complete outfits on various social media outlets from within the app. The app uses color combinations and popular styles to make clothing recommendations.
Our project is a mobile application (and a related backend API) that helps users avoid the frustration of having to color coordinate their outfits.


