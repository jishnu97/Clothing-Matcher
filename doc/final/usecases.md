
# Use Case 1: User Authentication / Create Account
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Mattin Zargarpur |
| Last Update: | 9/20/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Login system <br>Back-end server |
| Stakeholders and Interests | User: Wants an account so that they can use the application. Wants their information stored securely. <br>Company: Wants to sell clothes and make money. |
| Preconditions | User has opened application. <br>User has an internet connection. |
| Success Post Condition | User has created a new account. |
| Failed Post Condition | User has not created a new account and may not be able to use the application. |

## Main Success Scenario: Basic Flow

  1. User selects "create new account" on login page.
  2. Account creation page opens.
  3. User enters their email address, password, and password again to ensure correctness.
  4. User selects "submit".
  4. System verifies that the email address is not already in use.
  5. System verifies that the two entires of the user's password match.
  6. System creates an account with the specified email address and password.
  7. Login page opens.

## Extensions or Alternate Flows

  1. The email that the user entered is already in use.
       1. System prompts user that the email address provided is already in use.
       2. User enters another email address.
       3. User selects "submit".
  2. The two entries of the user's password do not match.
      1.  System prompts user that their password entries do not match.
      2.  User re-enters password.
      3.  User selects "submit".
  3.  User loses internet connection.
       1. User is notified that connection has been lost.
       2. User cannot proceed with account creation until connection is re-established.

## Special Requirements

  1. User password should not be displayed on the screen.
  2. User password should be handled securely, ensuring that applications on the user's phone and network cannot access it.

## Technology and Data Variations List

  1. None.

## Frequency of Occurrence
Mostly likely once per user.

## Other Issues

  1. Will there be character or length requirements for user passwords?

<div style="page-break-after: always;"></div>

# Use Case 2: Follow Brand Page
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Mattin Zargarpur |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Clothing companies <br>Back-end server |
| Stakeholders and Interests | User: Wants to be able to find companies that they are interested in.<br> Brands: Want to be able to advertise on the platform. <br>Company: Want users and companies to be able to interact on the platform. |
| Preconditions | User has been authenticated. (Use case 14) <br>User has an internet connection. |
| Success Post Condition | User is following the brand of their choice and is receiving promoted styles from them. |
| Failed Post Condition | User is not following the brand and is not receiving any information. |

## Main Success Scenario: Basic Flow

  1. User selects "search brands" on home screen.
  2. Brand search screen opens.
  3. User enters the name of the brand that they wish to follow.
  4. Search list of brands matching the user entry appears. Each list entry contains the brand name, logo, and a follow button.
  5. User selects the follow button for each brand that they wish to follow.

## Extensions or Alternate Flows

  1. The brand that they user wants to follow is not in the application.
      1. User is prompted that their entry does not match any of our listed brands and a list of suggested companies appears, instead.
  2. User loses internet connection.
      1. User is notified that connection has been lost.
      2. User can continue to scroll through the brands that are loaded in the list, but will not be able to view more or follow any until connection has been re-established.

## Special Requirements

  1. Search results should populate as the user types, promoting a responsive UI.
  2. The list of brands should be updated periodically to reflect the latest brands.

## Technology and Data Variations List

  1. None.

## Frequency of Occurrence
Frequently when an account is new as users search for brands they are already interested in, and then less frequently as users discover new brands.

## Other Issues

  1. How will company pages be managed?

<div style="page-break-after: always;"></div>

# Use Case 3: View Brands' Promoted Styles
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Mattin Zargarpur |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Feed system <br>Clothing companies <br>Back-end server |
| Stakeholders and Interests | User: Wants to receive promoted styles from all of the brands that they follow in a convenient format. <br>Brands: Want to be able to advertise to users. <br> Company: Wants to facilitate interaction between users and brands.|
| Preconditions | User has "followed" at least one brand. <br>See use case 2: Follow Brand Page. <br>User has an internet connection. <br>User is on the home screen. <br>User has been authenticated. (Use case 14)|
| Success Post Condition | User has seen promoted styles from the brands that they follow. |
| Failed Post Condition | The user can not view promoted styles. |

<div style="page-break-after: always;"></div>

## Main Success Scenario: Basic Flow

  1. Brand feed on home screen automatically populates with promoted styles from brands that the user follows.
  2. User scrolls through the list, each entry in the list contains the item name, a picture of the item, and brand.
  3. As the user approaches the bottom of the list, more promoted styles are added.

## Extensions or Alternate Flows

  1. At any time, the user can select any entry in the list.
     1. An item view page opens, presenting the user with multiple larger pictures of the item and a button to shop for the item.
     2. User selects the shop button.
     3. A page linking to where the user can purchase the item from opens.
     4. User can return to the same location in their feed.
 2. At any time, the user might lose their internet connection.
      1. User is prompted that there is no connection.
      2. User can still scroll through the portion of their feed that is already loaded.

## Special Requirements

  1. As the user scrolls through their feed and views items, loading should be quick enough to provide a nearly continuous experience.

## Technology and Data Variations List

  1. None.

## Frequency of Occurrence
Frequently, since this is what the user is presented with after logging in.

## Other Issues

  1. How will the user's feed be ordered?

<div style="page-break-after: always;"></div>

# Use Case 4: Choose Outfits on the basis of Time of Year
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Joseph Nuar |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Feed System <br>Back-end server<br> Wardrobe |
| Stakeholders and Interests | Users: Want to be able to choose their outfits. |
| Preconditions | User has internet connection. <br> User has been authenticated. (Use case 14)|
| Success Post Condition | User can see outfits based on time of year |
| Failed Post Condition | System tells user, search failed. <br>Goes back to please choose time of year for outfit. |

## Main Success Scenario: Basic Flow

  1. User selects to make outfits on the basis of time of the year from home screen.
  2. The field "Time of the year" is populated automatically based on the month.
  3. User selects show me outfits.
  3. Outfits are made on the basis of time of the year. If it is fall, darker colors and heavier fabrics are chosen. If it is winter, user is shown options for winter coats in addition to the base outfit. If it is spring, user is shown outfits made up of brighter colors. If it is summer, user is shown outfits made of lighter fabrics.
  3. The user is presented with outfits from the wardrobe and those that can be bought from stores or online. Preference is given to outfits already present in the wardrobe. In the event that an outfit cannot be made from items in the wardrobe, the user is shown options to buy articles that are missing.

## Extensions or Alternate Flows

  2. User can choose "Time of the year" manually from the following list:
      3. Fall
      4. Winter
      5. Spring
      6. Summer
  3. Use case 7, the user can share the outfit.
  4. Use case 16, the user can favorite outfits.
  4. At any time, the user might lose their internet connection.
      5. The user is notified that they have lost internet connection.
      1. The system returns the cache of the last page they were on.

## Special Requirements

  1. Pre-cache of items user is going to load to make loading time faster from the cache.

## Technology and Data Variations List

  1. User can enter the "Time of the year" manually.

## Frequency of Occurrence
Frequently, when user uses system to find a new outfit.

## Other Issues

  1. How will the outfits be ordered?

<div style="page-break-after: always;"></div>

# Use Case 5: Customize Profile
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Joseph Nuar |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Wardrobe <br>Back-end server |
| Stakeholders and Interests | User: wants to make it easy to be able to take the different outfits owned to be quickly accessible. <br>Company: wants to sell outfits and make money. |
| Preconditions | User is connected to the internet.<br> User has been authenticated. (Use case 14) |
| Success Post Condition | The appropriate modifications have been made to the user's profile.|
| Failed Post Condition | The required modifications have not been made. The user is asked to redo the operation.|

## Main Success Scenario: Basic Flow

  1. User chooses to customize profile from the home screen.
  2. User can add or remove preferred colors from this screen.
  3. User can add or remove preferred level of formality in this screen.
  4. User can add or remove preferred activities.
  5. User can add age and location.
  5. After finishing all modifications, user selects save to save the preferences.
  

## Extensions or Alternate Flows

  4. At any time, the user might lose their internet connection.
      5. The user is notified that they have lost internet connection.
      1. The system returns the cache of the last page they were on.


## Special Requirements

  1. Access to the internet for their profile to be saved to the server.

## Technology and Data Variations List

  1. The location field can be populated automatically or manually.
  2. The color field can be populated automatically using color recognition or manually. 

## Frequency of Occurrence
Infrequently, only times when they need to update their preferences.

## Other Issues

  1. Is the profile customizable offline?
  
<div style="page-break-after: always;"></div>

# Use Case 6: Outfit Suggestions 
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Joseph Nuar |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Wardrobe <br>Back-end server |
| Stakeholders and Interests | User: Wants to have outfit completetion or full outfits suggested to them. <br>Clothing Companies: Want their products to be suggested to users. <br>Company: Wants to provide users with helpful functionality to promote continued use of the application. |
| Preconditions | User has been authenticated. (Use case 14) <br> User is connected to the internet. |
| Success Post Condition | User has a completed outfit. |
| Failed Post Condition | The user is not provided with a completed outfit. |

## Main Success Scenario: Basic Flow

  1. User selects "suggest outfits" on home screen.
  2. Parameters page opens.
  3. User can select articles of clothing in their wardrobe to include in the suggestions.
  3. User can select the activity that the outfit is meant for.
  4. User selects "generate suggestions".
  5. System generates a list of outfits that match the supplied parameters and time of year using clothing from the user's wardrobe, followed clothing company pages, nearby retailers, and popular clothing items.
  6. System presents the list of outfit suggestions to the user.
  
## Extensions or Alternate Flows

  1. User selects an outfit from the list of suggestions.
	1. A popup opens, showing the outfit in more detail and listing the items that comprise it.
	2. User can return to the same position in the list.
  2. Internet connection is lost.
	1. User it notified that there is no connection.
	2. Application does not proceed until connection is re-established.

## Special Requirements

  2. Generation of suggestions must be quick to avoid frustration.
  3. Generated outfits must match user's personal preferences.

## Technology and Data Variations List

  None.

## Frequency of Occurrence
Frequently, since this is the primary functionality of the application.

## Other Issues

  2. How will generated outfits be presented to users?

<div style="page-break-after: always;"></div>

# Use Case 7: Share Outfits
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Joseph Nuar |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end server <br> Social media outlets <br> Wardrobe|
| Stakeholders and Interests | User: wants to be able to share their outfits. <br>Social media companies: their platforms will be used to share our users' content. <br>Company: Wants to promote the application through users sharing their outfits.|
| Preconditions | User is viewing an outfit, either saved or just created.<br>User has been authenticated. (Use case 14) |
| Success Post Condition | Successfully able to share outfit with another application or on social media. |
| Failed Post Condition | Outfit is not shared. |

## Main Success Scenario: Basic Flow

  1. User presses share button.
  2. Application creates an image of the outfit.
  3. Popup containing list of applications and social media platforms opens.
  4. User selects how they want to share the outfit.
  5. Relevant application opens and user completes share process.
  3. Popup remains open so that the user can share on another platform if they wish.

## Extensions or Alternate Flows

  1. User selects a social media platform at step 4.
	1. A page showing the image to be shared and a text box opens.
	2. The user can type into the text box to add a message.
	3. User selects the share button.
	4. The image and the user's message are shared on the selected platform.
  2. User selects an application at step 4 (eg. text message).
	1. Selected application opens with the image ready to be shared.
	2. User completes the process to share the image through the selected application.
  2. User selects a social media platform that is not tied to their account.
	1. Page presenting the user with a login screen for that social media platform opens.
	2. User completes login screen.
	3. Relevant social media account is tied to the user's account.
 2. Internet connection is lost.
	1. User it notified that there is no connection.
	2. Application does not proceed until connection is re-established.

## Special Requirements

 1. The list of applications and social media platforms is determined, in part, by what the user has installed on their phone.
  2. Must ensure a positive experience, regardless of what the user selects to share their outfit through.
  3. Image and message size must conform to the unique requirements of each social media platform.
 
## Technology and Data Variations List

1. User can choose any of the popular social media outlets (Facebook, Instagram, Twitter, etc.).
 
<div style="page-break-after: always;"></div>

## Frequency of Occurrence
Whenever the user decides to share an outfit.

## Other Issues

  1. What size should the image be?

<div style="page-break-after: always;"></div>

# Use Case 8: Search for Clothing Items in Nearby Stores
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Mattin Zargarpur |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Clothing brands <br> Back-end server <br>Google|
| Stakeholders and Interests | User: Want convenient shopping options for the clothing that they want to purchase. <br>Google: Their Shopping tool will be used to find nearby products. <br>Company: Want to ensure the search results match user expectations. |
| Preconditions | An outfit has been suggested to the user and they have chosen to purchase one of the recommended items.<br> User has an internet connection.<br>User has been authenticated. (Use case 14) |
| Success Post Condition | User is notified of where they can purchase the item that they are interested in. |

## Main Success Scenario: Basic Flow

  1. User selects "available nearby" when buying articles to complete outfits.
  2. System searches on Google Shopping for items matching the recommended item.
  3. Search results are presented to the user in the application.
  4. User can scroll through the list to view purchase options.
  5. As the user approaches the bottom of the list, more search results are added.

## Extensions or Alternate Flows

  1. The user can adjust search parameters, such as distance and retailer.
       1. The list of stores updates as the user changes parameters.
  2. The user can select any result and be shown a larger picture of the item, information about the retailer, and a button to open GPS navigation to the retailer.
       1. User selects GPS button.
       2. User's GPS application opens and sets a route to the retailer.
       3. User can close the item view and retain to the same location in the search results.
  4. User can select the location manually. 
  3. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User can continue to scroll through the currently loaded search results.

## Special Requirements

  1. Search results should populate quickly.

## Technology and Data Variations List

  1. The location can also be entered manually.

## Frequency of Occurrence
Whenever the user decides to purchase an item nearby.

<div style="page-break-after: always;"></div>

# Use Case 9: Choose outfits on the basis of activity
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Jishnu Renugopal |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end Server <br> Clothing companies<br>Wardrobe|
| Stakeholders and Interests | Clothing companies: Want to sell as many clothes as they can. <br> User: Wants to look their best for the desired activity. |
| Preconditions | The user has internet connection. <br>The user has been identified and authenticated. (Use case 14) |
| Success Post Condition | Outfits for the required activity are returned. |
| Failed Post Condition | The required outfits are not returned and the user is asked to restart the search. |

## Main Success Scenario: Basic Flow

  1. The home screen is displayed.
  2. The user chooses to make outfits using type of activity.
  2. The user selects the type of activity from the following list:
      1. Formal event.
      2. Business casual event.
      3. Casual event.
      4. Sports.
      5. Party.
  3. The user selects "Show me outfits".
  3. The user is presented with outfits from the wardrobe and those that can be bought from stores or online. Preference is given to outfits already present in the wardrobe. In the event that an outfit cannot be made from items in the wardrobe, the user is shown options to buy articles that are missing.


## Extensions or Alternate Flows

  1. The user can choose to add articles of clothing at any point during the search. (See use case 12)
  3. Use case 7, the user can share the outfit.
  4. Use case 16, the user can favorite outfits.
  3. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User can continue to scroll through the currently loaded search results.

## Special Requirements

  1. Results loaded from the internet populate quickly to avoid frustration.

## Technology and Data Variations List

  1. None.

## Frequency of Occurrence
Whenever the user decides to make outfits using the type of activity.

## Other Issues

  1. None.

<div style="page-break-after: always;"></div>

# Use Case 10: Coordinate Outfits Using Patterns
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Jishnu Renugopal |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end Server<br>Wardrobe<br>Clothing companies |
| Stakeholders and Interests | Clothing vendors: Want to sell as many clothes as they can. <br> User: Wants to look their best and not violate rules of fashion. |
| Preconditions | The user has internet connection.<br>The user has been identified and authenticated. (Use case 14) |
| Success Post Condition | Outfits that follow the rules of mixing patterns are returned. |
| Failed Post Condition | The required outfits are not returned and the user is asked to restart the search. |

## Main Success Scenario: Basic Flow

  1. The home screen is displayed.
  2. The user chooses to coordinate outfits using patterns.
  2. The user selects the type of the item from one of the following:
      1. Sports jacket.
      2. Pants.
      3. Shirt.
      4. Tie.
  5. The user selects the pattern.
  3. The user selects "Show me outfits".
  3. The user is presented with outfits from the wardrobe and those that can be bought from stores or online. Preference is given to outfits already present in the wardrobe. In the event that an outfit cannot be made from items in the wardrobe, the user is shown options to buy articles that are missing.


## Extensions or Alternate Flows

  1. The user can choose to add articles of clothing at any point during the search. (See use case 12)
  3. Use case 7, the user can share the outfit.
  4. Use case 16, the user can favorite outfits.
  2. The user can choose to set the pattern for more than one article of clothing.
       1. If possible, outfits are populated using preset values.
       2. If the combination violates rules of matching patterns, the user is asked to change the value of one of the fields.
  3. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User can continue to scroll through the currently loaded search results.

## Special Requirements

  1. Results loaded from the internet populate quickly to avoid frustration.
  2. An algorithm must be implemented using the rules mentioned in: http://effortlessgent.com/patterns-101-all-you-need-to-know-about-wearing-mixing-and-matching-patterns/ 
 1. System must support categorizing and labeling a variety of patterns.
 2. System must be able to differentiate between types of clothing articles and the components necessary to build a complete outfit.

## Technology and Data Variations List

 None.

C

## Frequency of Occurrence
Whenever the user decides to coordinate outfits using pattern.

## Other Issues

  1. Need to ensure that all or at least most of the possible patterns are covered.

<div style="page-break-after: always;"></div>

# Use Case 11: Find Colors Using Pictures
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Jishnu Renugopal |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end Server  |
| Stakeholders and Interests | Clothing vendors: Want to sell as many clothes as they can. <br>User: Wants to look their best for the desired activity. |
| Preconditions | The user has internet connection. <br>The user has been identified and authenticated. (Use case 14) |
| Success Post Condition | The correct color is recognized |
| Failed Post Condition | The correct color is not recognized and the user is asked to input the color manually. |

## Main Success Scenario: Basic Flow

  1. The user chooses to add an item to the wardrobe. (See use case 12)
  2. The user takes a picture of the item.
  3. The algorithm on the back-end server correctly recognizes the color of the item.
  2. The color of the item is used as the value for the field "Color".

<div style="page-break-after: always;"></div>

## Extensions or Alternate Flows

  1. The user can choose to select the color manually.
       1. The color is selected from a list containing the six primary colors.
  3. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User is asked to reconnect to the internet.

## Special Requirements

  1. The color recognition code should be able to consistently and accurately determine colors.

## Technology and Data Variations List

  1. The user should be able to select the color for articles of clothing manually.

## Frequency of Occurrence
Whenever the user decides to add a new item to the wardrobe.

## Other Issues

  1. How to ensure that the same color is returned for different lighting conditions?

<div style="page-break-after: always;"></div>

# Use Case 12: Add Items to Wardrobe
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Jishnu Renugopal |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end Server <br> Wardrobe|
| Stakeholders and Interests | Clothing vendors: Want to sell as many clothes as they can. <br>User: Wants to look their best for the desired activity. |
| Preconditions | The user has internet connection. <br>The user has been identified and authenticated. (Use case 14) |
| Success Post Condition | The required outfit is added. |
| Failed Post Condition | The required outfit is not added. The user is asked to redo the operation. |

## Main Success Scenario: Basic Flow

  1. The home screen is displayed.
  2. The user chooses to modify the wardrobe.
  3. The user selects "Add".
  2. The user selects the type of the item from one of the following:
      1. Jackets & Blazers.
      2. Coats.
      2. Pants.
      3. Shirts.
      4. Ties.
  3. The user takes a picture of the item and the color is recognized. (See use case 11)
  4. The user chooses the pattern. (See use case 10)
  3. The user selects "Confirm".

## Extensions or Alternate Flows

  1. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User is asked to reconnect to internet.

## Special Requirements

  1. The options for colors and patterns should be comprehensive enough that it covers most of the outfits.
 1. The wardrobe needs to be organized in an intuitive and easy to understand manner.

## Technology and Data Variations List
None.
  

## Frequency of Occurrence
Whenever the user decides to add a new item to the wardrobe.

## Other Issues

  1. Considerations as to how the wardrobe should be organized visually should be made.

<div style="page-break-after: always;"></div>

# Use Case 13: Remove Items from Wardrobe
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Jishnu Renugopal |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end Server <br>Wardrobe|
| Stakeholders and Interests | Clothing vendors: Want to sell as many clothes as they can. <br> User: Wants to look their best for the desired activity. |
| Preconditions | The user has internet connection. <br>The user has been identified and authenticated. (Use case 14) |
| Success Post Condition | The required outfit is removed. |
| Failed Post Condition | The required outfit is not removed. The user is asked to redo the operation. |

## Main Success Scenario: Basic Flow

  1. The home screen is displayed.
  2. The user chooses to modify the wardrobe.
  3. The user selects "Remove".
  4. The user chooses the item to be removed.
  3. The user selects "Confirm".

## Extensions or Alternate Flows

  1. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User is asked to reconnect to internet.
  1. User might not have any items in the wardrobe.
       1. User is notified that there are no items in the wardrobe.
       2. User is asked to add items to the wardrobe.
  2. If the user deletes an item by accident, the user can undo the operation.

## Special Requirements

  1. The items in the wardrobe have to be loaded quickly to avoid frustration.
  2. A local cache to provide a means to undo an unintended delete must be implemented.

## Technology and Data Variations List
None.
  

## Frequency of Occurrence
Whenever the user decides to remove an item from the wardrobe.

## Other Issues

  1. None.

<div style="page-break-after: always;"></div>

# Use Case 14: User Authentication / Sign In
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | James Taylor |
| Last Update: | 9/25/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Login system <br> Back-end server|
| Stakeholders and Interests | User: Wants an account to keep data secure and distinguish themselves from other users. <br>Company: Wants user to create account so that they can use the application. Wants to store information securely. |
| Preconditions | User has opened application. User has an internet connection. User has already created an account and successfully completed the application on-boarding flows |
| Success Post Condition | User has access to their account profile and data. User is able to use the application. |
| Failed Post Condition | User is continuously redirected to the login or signup screen. User is not able to use the application. |

## Main Success Scenario: Basic Flow

  1. User is presented a splash screen of the application, followed by a login screen.
  2. User enters their email address, password.
      1. Include alternate flow for registering an account in the displayed form
  3. User selects "login".
  4. System verifies that the email address is valid.
  5. System verifies that the provided password matches the password associated with the email address.
  6. Application stores the provided email and password token, creating a new session.
  7. Application loads user profile and data.
  8. Application transitions to home screen.

## Extensions or Alternate Flows

  1. The email that the user entered is not associated with an account.
        1. System prompts user that the email address provided is not associated with an account.
        2. System suggests that perhaps the user intended to create a new account instead.
        2. User enters another email address.
        3. User selects "login".

  2. The provided password does not match the saved password associated with the email address.
      1.  System prompts user that their password is not correct.
      2.  User re-enters password.
      3.  User selects "login".

  3. User loses internet connection.
      1. User is notified that connection has been lost.
      2. User can not proceed with the login process until connection is re-established.

  4. User has logged into the application before.
      1. On initial application load, a formerly provided and valid email and password combination is retried.
      2. System verifies the authentication is correct.
          1. Email is associated with an account.
          2. Provided password matches password on record.
      3. Application loads user profile and data, and continues the login flow

## Special Requirements

  1. User password should not be displayed on the screen.
  2. User password should be handled securely, ensuring that applications on the user's phone and network cannot access it.

<div style="page-break-after: always;"></div>

## Technology and Data Variations List

  1. Must be able to handle a variety of native forms of user input.

## Frequency of Occurrence
Each time the user launches the application cold; meaning each time the application is initially started on the device after having been closed.

## Other Issues

  1. Will authentication be rate limited to a certain number of attempts in a given time interval?

<div style="page-break-after: always;"></div>

# Use Case 15: Third Party Integrations
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | James Taylor |
| Last Update: | 9/25/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end server <br>Clothing companies |
| Stakeholders and Interests | User: Wants to see a variety of clothing options for sale. <br>Company: Wants to make the platform attractive from a user adoption standpoint and supply a number of third-party vendors <br>Brands: want to make their products available to as many people as possible |
| Preconditions | User has opened application. <br>User has an internet connection. <br>User has been authenticated. (Use case 14) |
| Success Post Condition | User is redirected to a vendor page where they can choose to purchase / view clothing options in more detail |
| Failed Post Condition | User cannot find clothing they would like to interact with further and the flow is omitted. |

## Main Success Scenario: Basic Flow

  1. Find an article of clothing through search or suggest from an alternative flow.
  2. Tap icon adjacent to the picture of the item.
  3. Application hands off control to the browse, which opens a link of the article of clothing from a third-party vendor site.
      1. User many purchase the article of clothing or view more information about it not displayed in the application

## Extensions or Alternate Flows

  1. The link is no longer valid.
      1. Upon handing off the request, the third-party website will display an appropriate application.
      2. Control can be returned to the application by switching back.
  2.  User loses internet connection.
      1. User is notified that connection has been lost.
      2. The link will fail to be opened from the application.
      3. An error message will be displayed

## Special Requirements

  1. Most, if not all articles of clothing displayed should automatically have third-party integration.

## Technology and Data Variations List

  1. System needs to gracefully handle redirection to a variety of vendor pages, all structured differently.

## Frequency of Occurrence
Each time the user wants to interact further with content displayed in the application.

## Other Issues

  1. How will the interface change for clothing that does not have external links?
  2. How will third-parties that don't support online purchases affect user experience? Should this be a detractor in other flows?

<div style="page-break-after: always;"></div>

# Use Case 16: Thumb Up / Favorite Clothing Items
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | James Taylor |
| Last Update: | 9/25/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end server <br>Wardrobe |
| Stakeholders and Interests | User: Want to express likes and dislikes. Want to provide information about preference to better tailor their experiences using the application. <br>Company: Wants to keep metrics on trending clothing and provide more relevant suggestions to the user to increase engagement. |
| Preconditions | User has opened application.<br> User has an internet connection. <br>User has been authenticated. (Use case 14) |
| Success Post Condition | User has liked an article of clothing; visual changes to the application, suggestion changes, clothing article appears on their profile. |
| Failed Post Condition | User receives an error message indicating they should try again later. |

## Main Success Scenario: Basic Flow

  1. User is in another application flow and notices an article of clothing they like.
  2. User taps the "like" icon next to the clothing item.
  3. System updates their favorite / likes list on their profile.
  4. System sends notification that the item has been liked.
  5. Application changes the appearance of the icon next to item.

## Extensions or Alternate Flows

  1. There is an internal problem that prevents the item from being liked.
      1. Application displays an error message indicating the user should try again later.
  2. User taps the like icon of an item they have already liked.
      1.  System silently ignores this.
  3. User refreshes the screen / performs an action that displays an item again
      1.  System should check if an item has already been liked and render it differently.
      2. In this way, items that have already been liked by the user should persist.
  4.  User loses internet connection.
      1. User is notified that connection has been lost.
      2. User cannot proceed until connection is re-established.

## Special Requirements

  1. User likes should be cached or checked before rendering each item, so that this state persists on a refresh of the application.
  2. User likes, along with other profile data, should be handled securely.
  1. Must be able to determine like status on all items through the application.

## Technology and Data Variations List

 None.

## Frequency of Occurrence
Every time the user taps the "like" icon next to an item.

## Other Issues

  1. Will there be support for "un-liking" an item?

<div style="page-break-after: always;"></div>

# Use Case 17: Weather Widget for Clothing Recommendations
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | James Taylor |
| Last Update: | 9/25/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end server <br>Wardrobe <br>Weather API provider |
| Stakeholders and Interests | User: Wants to quickly see appropriate outfit options based on the weather. Wants convenience and interesting suggestions to break routine. <br>Company: Wants to provide useful and relevant information for the user. Wants to engage the user and be part of their daily life. |
| Preconditions | User has an internet connection. <br>User has been authenticated. (Use case 14)|
| Success Post Condition | User will see a random outfit select, appropriate to the current weather conditions of the user's location |
| Failed Post Condition | Widget displays a relevant error message. |

<div style="page-break-after: always;"></div>

## Main Success Scenario: Basic Flow

  1. User adds widget natively to their phone's home screen / widget view.
  2. On widget screen load, the phone will invoke the widget.
  3. Widget will query weather data.
  4. Widget will gather location data.
  5. Widget will exchange data with application to select outfit.
      1. Application will authenticate
      2. Application will use profile data / favorites / trends to select clothing from the user's collection
  6. A relevant selection will display on screen

## Extensions or Alternate Flows

  1. Location data is unavailable.
      1. Error message is displayed prompting user to open settings and grant permission.
  2. Weather data is unavailable.
      1. Widget shows relevant error message.
      2. User re-enters password.
      3. User selects "submit".
  3. User is not authenticated with the application / there is no application session.
      1. Session will attempt to be created.
      2. Error message will be displayed, tell the user to login.
      3. Taping the widget will redirect the user to the application main login screen

## Special Requirements

  1. Suggestions shown should only include the collection of clothing owned by the user.
  2. System must incorporate a dependency to a reliable back-end API providing weather data.

## Technology and Data Variations List

  1. System must be able to support a variety of native phone extensions, iPhone and Android being the most important.

## Frequency of Occurrence
Widget will load each time the user natively goes to the widget screen on their phone (operating system dependent).

## Other Issues

  1. What other information can be shown to an authenticated user without any clothing added to their personal collection?

<div style="page-break-after: always;"></div>

# Use Case 18: Coordinate Outfits Using Colors
## General Characteristics

| Attribute  | Description |
|---:|---|
| Author | Jishnu Renugopal |
| Last Update: | 9/21/2016 |
| Scope | System |
| Level | User |
| Status | Finalized |
| Primary Actor | User |
| Secondary Actors | Back-end Server<br>Wardrobe<br>Clothing companies |
| Stakeholders and Interests | Clothing vendors: Want to sell as many clothes as they can. <br> User: Wants to look their best and not violate rules of fashion. |
| Preconditions | The user has internet connection.<br>The user has been identified and authenticated. (Use case 14) |
| Success Post Condition | Outfits that follow the rules of matching colors are returned. |
| Failed Post Condition | The required outfits are not returned and the user is asked to restart the search. |

## Main Success Scenario: Basic Flow

  1. The home screen is displayed.
  2. The user chooses to coordinate outfits using colors.
  2. The user selects the type of the item from one of the following:
      1. Sports jacket.
      2. Pants.
      3. Shirt.
      4. Tie.
  5. The user selects the color.
  3. The user selects "Show me outfits".
  3. The user is presented with outfits from the wardrobe and those that can be bought from stores or online. Preference is given to outfits already present in the wardrobe. In the event that an outfit cannot be made from items in the wardrobe, the user is shown options to buy articles that are missing.


## Extensions or Alternate Flows

  1. The user can choose to add articles of clothing at any point during the search. (See use case 12)
  2. Use case 7, the user can share the outfit.
  4. Use case 16, the user can favorite outfits.
  2. The user can choose to set the color for more than one article of clothing.
       1. If possible, outfits are populated using preset values.
       2. If the combination violates rules of matching colors, the user is asked to change the value of one of the fields.
  3. At any time, the user can lose internet connection.
       1. User is notified that there is no connection.
       2. User can continue to scroll through the currently loaded search results.

## Special Requirements

  1. Results loaded from the internet populate quickly to avoid frustration.
  2. An algorithm must be implemented using the rules mentioned in the following resource: http://www.realmenrealstyle.com/color-wheel-color-coordination-men/ 

## Technology and Data Variations List

  2. None
  
## Frequency of Occurrence
Whenever the user decides to coordinate outfits using colors.

## Other Issues
1. Ensure that all possible colors and color combinations are covered by the algorithm. 
  

