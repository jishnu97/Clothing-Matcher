
# Non-functional requirement 1: Security
Users must be able to create and authenticate accounts without risk of their login information being compromised. Any systems that handle this information must do so securely. Any personal information that users provide, such as name and images, must also be kept private unless they are shared by the user.

# Non-functional requirement 2: Reliability
The application needs to reliable such that it maintains a 90% uptime or greater. This is vital to ensure confidence amongst the users and clothing companies. This is also essential to prevent frustration among users by maintaining quick and continuous response. The user's local device needs to cache enough data such that a problem with the server does not affect the user's experience. This requirement will be evaluated before deployment.

# Non-functional requirement 3: Performance
The application needs to be able to handle 90% of the requests in 2 seconds or less. The performance of the application must be quick enough to query the server and to get information from the internet for it to be a success. This ensures that the user has a quick and responsive application when they use it. The users will get impatient if more than ten percent of requests take to long to perform. This requirement will be evaluated before deployment.

# Non-functional requirement 4: Error Generation and Code Robustness
The backend services powering the front-end application must reasonably reliable. This means there should be less than 2% HTTP 500 error rate generated from the RESTful API layer, evaluated over a 24 hour period. An unreliable or error-prone backend would break functionality for the application and, worse case, render the application unusable. Users would not be able to login, save their profile, or make queries into the database.
