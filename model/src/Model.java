import java.util.ArrayList;

public class Model {

	private static Model instance = null;
	private User user;
	public Model() {
		// Exists to defeat instantiation
	}
	
	public static Model getInstance() {
		if (instance == null) {
			instance = new Model();
		}
		return instance;
	}
	public User createUser(String emailID, String password)
	{
		return new User(emailID, password);
	}
	/*
	public boolean authenticate()
	{
		
	}
	public boolean createAccount(User user)
	{
		
	}*/
	public ArrayList<User.Color> getColor(User.Color color, User.Type type)
	{
		return user.getColorList(new Item(-1, color, type));
	}
	public boolean addItem(User.Color color, User.Type type)
	{
		//Add item to the database http
		return true;
	}
}
