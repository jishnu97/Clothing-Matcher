import java.util.ArrayList;


public class User {
	public static ArrayList<Color> colors;
	public enum Color{
		RED, YELLOW, BLUE, PURPLE, GREEN, ORANGE
	}
	public enum Type{
		SHIRT, PANT, JACKET, TIE
	}
	private String emailId;
	private String password;
	private ArrayList<Item> items;
	public User(String emailId, String password) {
		this.emailId = emailId;
		this.password = password;
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		colors.add(Color.RED);
		colors.add(Color.PURPLE);
		colors.add(Color.GREEN);
		colors.add(Color.ORANGE);
		items = new ArrayList<Item>();
	}
/*	public boolean validateUser()
	{
		return false;
	}*/
	
	public void addItem(Item item)
	{
		//HTTP call to add this item
		items.add(item);
	}
	
	public ArrayList<Item> getItems(){
		//HTTP list to get items associated with the user
		return items;
	}
	public ArrayList<Color> getColorList(Item item)
	{
		ArrayList<Color> colors = new ArrayList<Color>();
		for(int i=0;i<6;i++)
		{
			if(item.getColor()!=colors.get(i))
			{
				colors.add(colors.get(i));
			}
		}
		return colors;
	}
}
