
public class Item {
	private float price;
	private User.Color color;
	private User.Type type;

	public Item(float price, User.Color color, User.Type type) {
		this.setColor(color);
		this.setPrice(price);
		this.setType(type);
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public User.Color getColor() {
		return color;
	}

	public void setColor(User.Color color) {
		this.color = color;
	}

	public User.Type getType() {
		return type;
	}

	public void setType(User.Type type) {
		this.type = type;
	}
}
