package cs3704.clothing.Fragements;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import cs3704.clothing.MainActivity;
import cs3704.clothing.R;
import cs3704.clothing.logic.*;
//import cs3704.clothing.logic.User;

/**
 * Created by Mattin on 11/25/2016.
 */

public class SignupFragment extends Fragment
{
    public SignupFragment()
    {
        //Empty.
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

        final EditText userText = (EditText) rootView.findViewById(R.id.signup_user);
        final EditText pass1Text = (EditText) rootView.findViewById(R.id.signup_pass);
        final EditText pass2Text = (EditText) rootView.findViewById(R.id.signup_pass2);

        final Button submitButton = (Button) rootView.findViewById(R.id.signup_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(userText.getText().toString().equals("")
                        || pass1Text.getText().toString().equals("")
                        || pass2Text.getText().toString().equals(""))
                {
                    Toast.makeText(rootView.getContext(), "Please complete form", Toast.LENGTH_SHORT).show();
                }
                else if (!pass1Text.getText().toString().equals(pass2Text.getText().toString()))
                {
                    Toast.makeText(rootView.getContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    try {
                        Model.getInstance().createAccount("", "", userText.getText().toString(), pass1Text.getText().toString());

                        //Return to login screen.
                        getFragmentManager().popBackStack();
                    }
                    catch (IOException | org.json.JSONException e) {
                        //empty block
                    }
                }
            }
        });

        return rootView;
    }
}
