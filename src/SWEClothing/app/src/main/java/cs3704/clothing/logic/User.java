package cs3704.clothing.logic;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class User {
	public static ArrayList<String> colors;
	private String emailId;
	private String password;

	public User(String emailId, String password) throws IOException {
		this.emailId = emailId;
		this.password = password;
		colors = new ArrayList<String>();
		colors.add("blue");
		colors.add("yellow");
		colors.add("red");
		colors.add("purple");
		colors.add("green");
		colors.add("orange");
		colors.add("white");
		colors.add("black");
		colors.add("brown");
	}

	public boolean validateUser() throws IOException, JSONException {
		JsonReader.open();
		JsonReader.Post("http://cs37046.nfshost.com/login", this.makeJSON());
		return !JsonReader.handlerJSON("http://cs37046.nfshost.com/profile")
				.equals("Unauthorized");
	}

	private JSONObject makeJSON() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("email", this.emailId);
		json.put("password", this.password);
		return json;
	}

	public int addItem(Item item) throws IOException, JSONException {
		validateUser();
		int res = JsonReader.Post("http://cs37046.nfshost.com/profile/wardrobe",
				item.toJson());
		JsonReader.close();
		return res;
	}

	private Item makeItem(JSONObject item) throws JSONException {
		double price = item.getDouble("price");
		String color = item.getString("color");
		int type = item.getInt("type");
		String maker = item.getString("maker");
		String name = item.getString("name");
		return new Item(price, color, type, maker, name);
	}

	public ArrayList<Item> getItems() throws ClientProtocolException,
			JSONException, IOException {
		validateUser();
		ArrayList<Item> items = new ArrayList<Item>();
		JSONArray arr = JsonReader
				.getArray("http://cs37046.nfshost.com/profile/wardrobe");
		for (int i = 0; i < arr.length(); i++) {
			items.add(makeItem(arr.getJSONObject(i)));
		}
		JsonReader.close();
		return items;
	}

	public ArrayList<Item> getColorList(Item item) {
		ArrayList<Item> items = new ArrayList<Item>();
		ArrayList<Integer> outfitTypes = new ArrayList<Integer>();
		// Adds all components of the outfit
		outfitTypes.add(0);
		outfitTypes.add(1);
		outfitTypes.add(2);
		outfitTypes.add(5);
		outfitTypes.remove(new Integer(item.getType()));
		String[] names = { "Shirt", "Pant", "Jacket", "Tie" };
		// Sets up a blacklist for colors
		ArrayList<String> colorBlackList = new ArrayList<String>();
		colorBlackList.add(item.getColor());
		double price = 21.99;
		String maker = "Ralph Lauren";
		for (Integer counter : outfitTypes) {
			String color = getColor(colorBlackList);
			String name = null;
			if (counter == 5) {
				name = names[3];
			} else {
				name = names[counter];
			}

			Item tempItem = new Item(price, color, counter, maker, name);
			items.add(tempItem);
			colorBlackList.add(tempItem.getColor());
			price += 1.27;
			maker = "IZOD";
		}

		items.add(item);

		return items;
	}

	/*
	 * Returns the appropriate color
	 */
	public String getColor(ArrayList<String> colorBlackList) {
		for (int i = 0; i < 9; i++) {
			if (!colorBlackList.contains(colors.get(i))) {
				return colors.get(i);
			}
		}
		return null;
	}
}
