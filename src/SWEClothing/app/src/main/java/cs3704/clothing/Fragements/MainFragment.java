package cs3704.clothing.Fragements;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.StrictMode;
import cs3704.clothing.R;

/**
 * Created by Mattin on 11/23/2016.
 */

public class MainFragment extends Fragment {

    public MainFragment()
    {
        //Empty Constructor required for fragment subclasses.
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        /*if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        } */
        return inflater.inflate(R.layout.fragment_main, container, false);
    }
}
