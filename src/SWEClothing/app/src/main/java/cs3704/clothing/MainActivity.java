package cs3704.clothing;

import android.app.Fragment;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import cs3704.clothing.Fragements.AddItemFragment;
import cs3704.clothing.Fragements.LoginFragment;
import cs3704.clothing.Fragements.MainFragment;
import cs3704.clothing.Fragements.OutfitFragment;
import cs3704.clothing.logic.User;

public class MainActivity extends AppCompatActivity {

    private String[] mNavOptions;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Init members
        mNavOptions = getResources().getStringArray(R.array.nav_drawer_options_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout) ;
        mDrawerList = (ListView) findViewById(R.id.nav_drawer);

        //Populates the Nav bar
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mNavOptions));
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                selectItem(i);
            }
        });

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        showLogin();
    }

    private void showLogin()
    {
        LoginFragment login = new LoginFragment();
        getFragmentManager().beginTransaction().replace(R.id.content_frame, login).commit();
        setTitle("Login");
    }

    public void setLoggedIn(User user)
    {
        mUser = user;
        selectItem(0);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void selectItem(int position)
    {
        //Toast.makeText(getApplicationContext(), "" + position, Toast.LENGTH_SHORT).show();

        Fragment fragment;
        switch (position)
        {
            case 1:
                fragment = new AddItemFragment();
                break;
            case 2:
                fragment = new OutfitFragment();
                break;
            default:
                fragment = new MainFragment();
        }

        getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        mDrawerList.setItemChecked(position, true);
        setTitle(mNavOptions[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    public User getUser()
    {
        return mUser;
    }
}
