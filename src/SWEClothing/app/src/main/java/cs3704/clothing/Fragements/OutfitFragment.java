package cs3704.clothing.Fragements;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import cs3704.clothing.MainActivity;
import cs3704.clothing.R;
import cs3704.clothing.logic.Item;
import cs3704.clothing.logic.Model;
import cs3704.clothing.logic.User;

/**
 * Created by Mattin on 11/23/2016.
 */

public class OutfitFragment extends Fragment
{
    private View mRootView;
    private String[] mItems;
    private ListView mItemList;

    private String mSelectedItem;

    public OutfitFragment()
    {
        //Empty.
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mRootView = inflater.inflate(R.layout.fragment_outfit, container, false);

        mSelectedItem = null;

        //Populates the wardrobe item list
        mItems = makeListItems(getItems());
        mItemList = (ListView) mRootView.findViewById(R.id.out_list);
        mItemList.setAdapter(new ArrayAdapter<String>(mRootView.getContext(), R.layout.drawer_list_item, mItems));

        mItemList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                selectItem(i);
            }
        });

        Button submitButton = (Button) mRootView.findViewById(R.id.out_submit_button);
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                submit();
            }
        });


        return mRootView;
    }

    private String[] getItems()
    {
        String[] returnData = null;
        try {
            //int dataSize = ((MainActivity) getActivity()).getUser().getItems().size();
            //returnData = new String[dataSize];
            ArrayList<Item> items = ((MainActivity) getActivity()).getUser().getItems();
            String[] types = getResources().getStringArray(R.array.styles_array);
            returnData = new String[items.size()];
            for (int i = 0; i < items.size(); i++)
            {
                returnData[i] = items.get(i).getName()
                        +"," +items.get(i).getMaker()
                        +"," +items.get(i).getColor()
                        +"," +types[items.get(i).getType() + 1]
                        +"," + items.get(i).getPrice();
            }
        }
        catch (IOException | org.json.JSONException e) {
            //empty block
        }
        return returnData;
    }

    private String[] makeListItems(String[] items)
    {
        String[] listItems = new String[items.length + 2];
        listItems[0] = getResources().getString(R.string.out_list_anon);
        listItems[1] = getResources().getString(R.string.out_list_new);
        for (int idx = 2; idx < listItems.length; idx++)
        {
            String[] attr = items[idx - 2].split(",");
            listItems[idx] = attr[0] + "\n"
                + attr[1] + "\n"
                + attr[2] + " " + attr[3] + "\n"
                + "$" + attr[4];
        }
        return listItems;
    }

    private void selectItem(int position)
    {
        mSelectedItem = null;

        if(position == 0)
        {
            //Anonymous item.
            AnonItemFragment anonDialog = new AnonItemFragment();
            anonDialog.setTargetFragment(this, 1);
            anonDialog.show(getFragmentManager(), "fragment_anon_item");
        }
        else if(position == 1)
        {
            //New item. Simply redirects to new item page.
            ((MainActivity)getActivity()).selectItem(1);
        }
        else
        {
            String[] splitItem = mItems[position].split("\n");
            splitItem = splitItem[2].split(" ");
            mSelectedItem = splitItem[1] + "," + splitItem[0];
        }


        mItemList.setItemChecked(position, true);
    }

    private void submit()
    {
        if(mSelectedItem == null)
        {
            //Error message
            Toast.makeText(mRootView.getContext(), "Please select an item.", Toast.LENGTH_SHORT).show();
        }
        else
        {
            //Display list of outfits.
            ArrayList<Item> items = new ArrayList<>();
            String[] splitItem = mSelectedItem.split(",");
            try {
                items = Model.getInstance().getColor(((MainActivity) getActivity()).getUser(), splitItem[1], getTypeNumber(splitItem[0]));
            }
            catch (Exception e) {
                //empty block
            }

            displayOutfit(items);
        }
    }

    private void displayOutfit(ArrayList<Item> outfit)
    {
        String message = "";
        for(Item i : outfit)
        {
            String itemDescription = "- " + i.getName() + "\n"
                    + i.getMaker() + "\n"
                    + i.getColor() + "\n"
                    + "$" + i.getPrice() + "\n";
            message = message + itemDescription;
            //message = message + "- " + i.toString() + "\n";
        }

        //Build dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(mRootView.getContext());
        builder.setTitle("Suggested Outfit");
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.create().show();
    }

    public void setAnonymousItem(String item)
    {
        String label = getResources().getString(R.string.out_list_anon) + ":\n";
        String[] splitItem = item.split(",");
        label = label + splitItem[1] + " " + splitItem[0];
        mItems[0] = label;

        mItemList.setAdapter(new ArrayAdapter<String>(mRootView.getContext(), R.layout.drawer_list_item, mItems));
        mItemList.setItemChecked(0, true);
        mSelectedItem = item;
    }

    private int getTypeNumber(String type)
    {
        int articleType = 0;
        String[] types = getResources().getStringArray(R.array.styles_array);
        for(int idx = 1; idx < types.length; idx++)
        {
            if(type.equals(types[idx]))
            {
                articleType = idx - 1;
            }
        }

        return articleType;
    }
}
