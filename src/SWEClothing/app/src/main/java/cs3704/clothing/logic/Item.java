package cs3704.clothing.logic;
import org.json.JSONException;
import org.json.JSONObject;

public class Item {
	private double price;
	private String color;
	private int type;
	private String maker;
	private String name;

	public Item(double price, String color, int type, String maker, String name) {
		this.setColor(color);
		this.setPrice(price);
		this.setType(type);
		this.setMaker(maker);
		this.setName(name);
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public JSONObject toJson() throws JSONException {
		JSONObject res = new JSONObject();
		res.put("price", this.price);
		res.put("name", this.name);
		res.put("maker", this.maker);
		res.put("type", this.getType());
		res.put("color", this.getColor());
		return res;
	}

	public String toString() {
		return maker + " " + name + " " + color + " " + type + " "
				+ price ;
	}

}
