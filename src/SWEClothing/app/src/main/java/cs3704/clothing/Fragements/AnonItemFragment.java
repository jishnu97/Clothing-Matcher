package cs3704.clothing.Fragements;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import cs3704.clothing.R;

/**
 * Created by Mattin on 11/25/2016.
 */

public class AnonItemFragment extends DialogFragment
{
    private Spinner mStyleSpinner;
    private Spinner mColorSpinner;

    public AnonItemFragment()
    {
        //Empty.
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_anon_item, container);
        getDialog().setTitle(R.string.out_list_anon);

        mStyleSpinner = (Spinner) rootView.findViewById(R.id.anon_style_spinner);
        mColorSpinner = (Spinner) rootView.findViewById(R.id.anon_color_spinner);
        Button acceptButton = (Button) rootView.findViewById(R.id.anon_submit_button);
        acceptButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mStyleSpinner.getSelectedItem().toString().equals("")
                        || mColorSpinner.getSelectedItem().toString().equals(""))
                {
                    Toast.makeText(view.getContext(), "Please complete form", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    handleClick();
                }
            }
        });

        return rootView;
    }

    private void handleClick()
    {
        String choice = mStyleSpinner.getSelectedItem().toString() + ","
                + mColorSpinner.getSelectedItem().toString();
        ((OutfitFragment)getTargetFragment()).setAnonymousItem(choice);
        this.dismiss();
    }

}
