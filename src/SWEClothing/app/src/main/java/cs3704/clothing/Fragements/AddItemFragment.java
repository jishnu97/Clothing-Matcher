package cs3704.clothing.Fragements;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;

import cs3704.clothing.MainActivity;
import cs3704.clothing.R;
import cs3704.clothing.logic.Model;
import cs3704.clothing.logic.User;

/**
 * Created by Mattin on 11/23/2016.
 */

public class AddItemFragment extends Fragment
{
    public AddItemFragment()
    {
        //Empty.
    }

    public View onCreateView(LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_add_item, container, false);

        //Init members
        final EditText nameText = (EditText) rootView.findViewById(R.id.add_name);
        final EditText brandText = (EditText) rootView.findViewById(R.id.add_brand);
        final EditText priceText = (EditText) rootView.findViewById(R.id.add_price);
        final Spinner typeSpinner = (Spinner) rootView.findViewById(R.id.add_type);
        final Spinner colorSpinner = (Spinner) rootView.findViewById(R.id.add_color);

        //Add Listener to add button
        Button submitButton = (Button) rootView.findViewById(R.id.add_button_submit);
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(nameText.getText().toString().equals("")
                        || brandText.getText().toString().equals("")
                        || priceText.getText().toString().equals("")
                        || typeSpinner.getSelectedItem().toString().equals("")
                        || colorSpinner.getSelectedItem().toString().equals(""))
                {
                    Toast.makeText(rootView.getContext(), "Must complete form", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    try {
                        Model.getInstance().addItem(
                                ((MainActivity) getActivity()).getUser(),
                                colorSpinner.getSelectedItem().toString(),
                                getTypeNumber(typeSpinner.getSelectedItem().toString()),
                                nameText.getText().toString(),
                                brandText.getText().toString(),
                                Float.parseFloat(priceText.getText().toString())
                        );
                    }
                    catch (IOException e) {
                        //empty block
                    }
                    catch (org.json.JSONException e) {
                        //empty block
                    }



                    Toast.makeText(rootView.getContext(), "Item Added", Toast.LENGTH_SHORT).show();

                    //Reset fields
                    nameText.setText("");
                    brandText.setText("");
                    priceText.setText("");
                    typeSpinner.setSelection(0);
                    colorSpinner.setSelection(0);

                }
            }
        });

        return rootView;
    }

    private int getTypeNumber(String type)
    {
        int articleType = 0;
        String[] types = getResources().getStringArray(R.array.styles_array);
        for(int idx = 1; idx < types.length; idx++)
        {
            if(type.equals(types[idx]))
            {
                articleType = idx - 1;
            }
        }

        return articleType;
    }
}
