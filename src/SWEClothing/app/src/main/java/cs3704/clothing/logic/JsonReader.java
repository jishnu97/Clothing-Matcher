package cs3704.clothing.logic;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class JsonReader {

	public static CloseableHttpClient httpClient = null;
	/*
	 * Opens a http connection
	 */
	public static void open() {
		httpClient =  HttpClientBuilder.create().build();
	}
	/*
	 * Closes the http connection
	 */
	public static void close() throws IOException {
		if(httpClient!=null)
		httpClient.close();
		httpClient = null;
	}
	/*
	 * Gets a JSON object from the specified URL
	 */
	public static JSONObject getJSON(String url)
			throws UnsupportedOperationException, IOException, JSONException {
		return new JSONObject(handlerJSON(url));

	}
	/*
	 * Handles JSON requests
	 */
	public static String handlerJSON(String url) throws ClientProtocolException, IOException {
		HttpGet request = new HttpGet(url);
		// add request header
		request.addHeader("User-Agent", "");
		CloseableHttpResponse response = httpClient.execute(request);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		//Returns the string which represents a JSON array
		return result.toString();
	}
	/*
	 * Gets an array of JSON objects
	 */
	public static JSONArray getArray(String url) throws ClientProtocolException, JSONException, IOException {
		return new JSONArray(handlerJSON(url));
	}
	/*
	 * Posts the given JSON to the specified URL
	 */
	public static int Post(String url, JSONObject json) throws IOException {
		int code = -1;
		try {
			HttpPost request = new HttpPost(url);
			StringEntity params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			CloseableHttpResponse response = httpClient.execute(request);
			code = response.getStatusLine().getStatusCode();
		} catch (Exception ex) {
			// handle exception here
		}
		return code;

	}

}