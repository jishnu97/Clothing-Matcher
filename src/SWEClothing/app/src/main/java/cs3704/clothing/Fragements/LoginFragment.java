package cs3704.clothing.Fragements;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import cs3704.clothing.MainActivity;
import cs3704.clothing.R;
import cs3704.clothing.logic.Model;
import cs3704.clothing.logic.User;

/**
 * Created by Mattin on 11/25/2016.
 */

public class LoginFragment extends Fragment {

    public static User userApp;

    public LoginFragment()
    {
        //Empty.
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        final EditText userText = (EditText) rootView.findViewById(R.id.login_user_name);
        final EditText passText = (EditText) rootView.findViewById(R.id.login_password);

        Button submitButton = (Button) rootView.findViewById(R.id.login_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(userText.getText().toString().equals("")
                        || passText.getText().toString().equals(""))
                {
                    Toast.makeText(rootView.getContext(), "Please complete form.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    try {
                        User user = Model.getInstance().createUser(userText.getText().toString(), passText.getText().toString());
                        if (Model.getInstance().authenticate(user))
                        {
                            ((MainActivity) getActivity()).setLoggedIn(user);
                        }
                        else
                        {
                            Toast.makeText(rootView.getContext(), "Invalid login credentials.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (IOException | org.json.JSONException e) {
                        //empty block
                    }
                }
            }
        });
        
        Button signupButton = (Button) rootView.findViewById(R.id.login_signup);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignupFragment signupFragment = new SignupFragment();
                getFragmentManager().beginTransaction().addToBackStack("mattinIsCool").replace(R.id.content_frame, signupFragment).commit();
            }
        });

        return rootView;
    }
}
