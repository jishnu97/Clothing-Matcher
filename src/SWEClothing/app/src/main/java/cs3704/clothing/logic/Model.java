package cs3704.clothing.logic;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public class Model {

	private static Model instance = null;

	public Model() {
		// Exists to defeat instantiation
	}

	public static Model getInstance() {
		if (instance == null) {
			instance = new Model();
		}
		return instance;
	}

	public User createUser(String emailID, String password) throws IOException {
		return new User(emailID, password);
	}

	public boolean authenticate(User user) throws IOException, JSONException {
		return user.validateUser();
	}

	public int createAccount(String firstName, String lastName, String email,
			String password) throws JSONException, IOException {
		JsonReader.open();
		JSONObject json = new JSONObject();
		json.put("firstname", firstName);
		json.put("lastname", lastName);
		json.put("email", email);
		json.put("password", password);
		int res = JsonReader.Post("http://cs37046.nfshost.com/signup", json);
		JsonReader.close();
		return res;
	}

	public ArrayList<Item> getColor(User user,String color, int type) {
		return user.getColorList(new Item(9.65, color, type, "Perry Ellis", "My Item"));
	}

	public void addItem(User user, String color, int type,String name, String maker, float price) throws IOException, JSONException {
		user.addItem(new Item(price, color, type, maker, name));
	}

}
