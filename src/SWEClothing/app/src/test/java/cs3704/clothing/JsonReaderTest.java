package cs3704.clothing;
import static org.junit.Assert.*;

import java.io.IOException;
import cs3704.clothing.logic.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class JsonReaderTest {

	/*
	 * Tests the get method
	 */
	@Test
	public void testGet() throws JSONException, IOException {
		JSONObject json = new JSONObject();
		json.put("firstname", "ccc");
		json.put("lastname", "ddd");
		json.put("email", "yes@vt.edu");
		json.put("password", "test6");
		JsonReader.open();
		JsonReader.Post("http://cs37046.nfshost.com/signup", json);
		assertTrue(JsonReader.getJSON("http://cs37046.nfshost.com/profile")
				.get("last_name").equals(json.get("lastname")));
		assertTrue(JsonReader.getJSON("http://cs37046.nfshost.com/profile")
				.get("first_name").equals(json.get("firstname")));
		assertTrue(JsonReader.getJSON("http://cs37046.nfshost.com/profile")
				.get("email").equals(json.get("email")));
		JsonReader.close();
	}
	/*
	 * Tests the post method
	 */
	@Test
	public void testPost() throws JSONException, IOException {
		JSONObject json = new JSONObject();
		json.put("firstname", "fff");
		json.put("lastname", "ggg");
		json.put("email", "success@vt.edu");
		json.put("password", "test6");
		JsonReader.open();
		assertTrue(JsonReader.Post("http://cs37046.nfshost.com/signup", json) == 302);
		JsonReader.close();
	}
	/*
	 * Tests the open() method
	 */
	@Test
	public void testOpen() throws IOException {
		JsonReader.open();
		assertTrue(JsonReader.httpClient!=null);
		JsonReader.close();
	}
	/*
	 * Tests the close() method
	 */
	@Test
	public void testClose() throws IOException {
		JsonReader.open();
		JsonReader.close();
		assertTrue(JsonReader.httpClient==null);
	}

}
