package cs3704.clothing;
import static org.junit.Assert.*;
import cs3704.clothing.logic.*;
import org.json.JSONException;
import org.junit.Test;

public class ItemTest {

	@Test
	/*
	 * Tests the toJSON() method
	 */
	public void testToJSON() throws JSONException {
		Item item = new Item(9.99, "blue", 0, "CK", "Shirt");
		assertTrue(item
				.toJson()
				.toString()
				.equals("{\"color\":\"blue\",\"price\":9.99,\"name\":"
						+ "\"Shirt\",\"maker\":\"CK\",\"type\":0}"));
	}
	/*
	 * Tests the toString() method
	 */
	@Test
	public void testToString() throws JSONException {
		Item item = new Item(9.99, "blue", 0, "CK", "Shirt");
		System.out.print(item
				.toString());
		assertTrue(item
				.toString()
				.equals("CK Shirt blue 0 9.99"));
	}
}
