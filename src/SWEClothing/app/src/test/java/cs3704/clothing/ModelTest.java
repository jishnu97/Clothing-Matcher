package cs3704.clothing;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import cs3704.clothing.logic.*;
import org.json.JSONException;
import org.junit.Test;

public class ModelTest {
	private Model testModel;

	// assigning the values
	protected void setUp() {
		testModel = Model.getInstance();
	}

	/*
	 * Test account creation use case
	 */
	@Test
	public void testCreateAccount() throws IOException, JSONException {
		setUp();
		assertTrue(testModel.createAccount("jishnu", "renugopal",
				"jishnu@vt.edu", "ammu123") == 302);
	}

	/*
	 * Test Login use case
	 */
	@Test
	public void testLogin1() throws IOException, JSONException {
		setUp();
		assertTrue(testModel.authenticate(new User("jishnu@vt.edu", "ammu123")));
	}

	/*
	 * Test Login use case for failure
	 */
	@Test
	public void testLogin2() throws IOException, JSONException {
		setUp();
		assertFalse(testModel.authenticate(new User("gibberish@vt.edu",
				"ammu123")));
	}

	/*
	 * Tests add Item use case
	 */
	@Test
	public void testAddItem() throws IOException, JSONException {
		User testUser = new User("jishnu@vt.edu", "ammu123");
		int prevSize = testUser.getItems().size();
		Model.getInstance().addItem(testUser, "blue", 2, "Shirt", "CK", 87);
		int newSize = testUser.getItems().size();
		assertTrue(prevSize +1== newSize);
	}
	/*
	 * Tests get color use case
	 */
	@Test
	public void testGetColor() throws IOException, JSONException {
		User testUser = new User("jishnu@vt.edu", "ammu123");
		ArrayList<Item> testItems = Model.getInstance().getColor(testUser, "blue", 2);
		assertTrue(testItems.size()==4);
		assertTrue(testItems.get(0).getType() == 0);
		assertTrue(testItems.get(1).getType() == 1);
		assertTrue(testItems.get(2).getType() == 5);
		assertTrue(testItems.get(0).getMaker().equals("Ralph Lauren"));
		assertTrue(testItems.get(1).getMaker().equals("IZOD"));
		assertTrue(testItems.get(2).getMaker().equals("IZOD"));
	}

}
