package cs3704.clothing;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import cs3704.clothing.logic.*;
import org.json.JSONException;
import org.junit.Test;


public class UserTest {
	private User testUser;
	protected void setUp() throws IOException
	{
		testUser = new User("jishnu@vt.edu", "ammu123");
	}
	
	/*
	 * Tests validate for success
	 */
	@Test
	public void testValidate1() throws IOException, JSONException
	{
		setUp();
		assertTrue(testUser.validateUser());
	}
	/*
	 * Tests validate for failure
	 */
	@Test
	public void testValidate2() throws IOException, JSONException
	{
		testUser = new User("jishnu@vt.edu", "ammu1253");
		assertFalse(testUser.validateUser());
	}
	/*
	 * Tests get items
	 */
	@Test
	public void testGetItems() throws IOException, JSONException
	{
		setUp();
		testUser.addItem(new Item(5.99, "blue", 5, "Ralph Lauren", "Silk Tie"));
		System.out.println(testUser.getItems().toString());
		assertTrue(testUser.getItems().get(0).getMaker().equals("CK"));
		assertTrue(testUser.getItems().get(0).getName().equals("Shirt"));
		assertTrue(testUser.getItems().get(0).getColor().equals("blue"));
	}
	/*
	 * Tests add item
	 */
	@Test
	public void testAddItem() throws IOException, JSONException
	{
		setUp();
		int prevSize = testUser.getItems().size();
		testUser.addItem(new Item(87.64, "blue", 2, "CK", "Shirt"));
		int newSize = testUser.getItems().size();
		assertTrue(prevSize +1== newSize);
		
	}
	
	/*
	 * Tests colorList() for shirt
	 */
	@Test
	public void getColorListTest1() throws IOException
	{
		setUp();
		ArrayList<Item> testItems = testUser.getColorList(new Item(5.99, "blue", 0, "CK", "shirt"));
		assertTrue(testItems.size()==4);
		assertTrue(testItems.get(0).getMaker().equals("Ralph Lauren"));
		assertTrue(testItems.get(1).getMaker().equals("IZOD"));
		assertTrue(testItems.get(2).getMaker().equals("IZOD"));
	}
	/*
	 * Tests colorList() for pant
	 */
	@Test
	public void getColorListTest2() throws IOException
	{
		setUp();
		ArrayList<Item> testItems = testUser.getColorList(new Item(5.99, "blue", 1, "CK", "pant"));
		assertTrue(testItems.size()==4);
		assertTrue(testItems.get(0).getMaker().equals("Ralph Lauren"));
		assertTrue(testItems.get(1).getMaker().equals("IZOD"));
		assertTrue(testItems.get(2).getMaker().equals("IZOD"));
	}
	/*
	 * Tests colorList() for jacket
	 */
	@Test
	public void getColorListTest3() throws IOException
	{
		setUp();
		ArrayList<Item> testItems = testUser.getColorList(new Item(5.99, "blue", 2, "CK", "jacket"));
		assertTrue(testItems.size()==4);
		assertTrue(testItems.get(0).getType() == 0);
		assertTrue(testItems.get(1).getType() == 1);
		assertTrue(testItems.get(2).getType() == 5);
		assertTrue(testItems.get(0).getMaker().equals("Ralph Lauren"));
		assertTrue(testItems.get(1).getMaker().equals("IZOD"));
		assertTrue(testItems.get(2).getMaker().equals("IZOD"));
	}
	/*
	 * Tests colorList() for tie
	 */
	@Test
	public void getColorListTest4() throws IOException
	{
		setUp();
		ArrayList<Item> testItems = testUser.getColorList(new Item(5.99, "blue", 5, "CK", "tie"));
		assertTrue(testItems.size()==4);
		assertTrue(testItems.get(0).getType() == 0);
		assertTrue(testItems.get(1).getType() == 1);
		assertTrue(testItems.get(2).getType() == 2);
		assertTrue(testItems.get(0).getMaker().equals("Ralph Lauren"));
		assertTrue(testItems.get(1).getMaker().equals("IZOD"));
		assertTrue(testItems.get(2).getMaker().equals("IZOD"));
	}
	
	
}
