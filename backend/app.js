// Dependencies
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');

// Passport configuration
require('./config/passport')(passport);

const app = require('express')();

app.disable('x-powered-by');

app.set('view engine', 'ejs');

// Define middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: 'FHAzGWKLCkrNXQQ9d2Kit560bVyjGXENRwT0h7uP'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/', (req, res, next) => {
    console.log(
        req.method,
        req.url,
        '-', new Date(), '-',
        req.connection.remoteAddress
    );

    next();
});

// Define routes
require('./app/routes')(app, passport);

// Handle 404
app.use((req, res, next) => {
    res.status(400);
    res.send('Not found');
});

// Handle 500
app.use((error, req, res, next) => {
    res.status(500);
    res.send('Internal server error');
});

app.listen(8080);

module.exports = app;
