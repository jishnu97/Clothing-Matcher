const express = require('express');
const path = require('path');
const https = require('https');

var mysql = require('promise-mysql');
var connection;
mysql.createConnection({
    host:'localhost',
    user: 'root',
    database: 'auth'
}).then(function(conn){
    connection = conn;
});

module.exports = (app, passport) => {

    /** Authentication specific routes */
    app.get('/logout', (req, res) => {
        req.logout();
        res.redirect('/login');
    });

    app.get('/login', (req, res) => {
        res.render('login.ejs', {
            message: req.flash('message')
        });
    });

    app.post('/login',
        passport.authenticate('local-login', {
            successRedirect: '/profile',
            failureRedirect: '/profile',
            failureFlash: true,
            badRequestMessage: 'Enter your account credentials to login'
        })
    );

    app.get('/signup', (req, res) => {
        res.render('signup.ejs', {
            message: req.flash('message')
        });
    });

    app.post('/signup',
        passport.authenticate('local-signup', {
            successRedirect: '/',
            failureRedirect: '/signup',
            failureFlash: true,
            badRequestMessage: 'Enter your account credentials to signup'
        })
    );

    /** User specific routes */
    app.get('/profile', enforceAuth, (req, res) => {
        const user = {
            email: req.user.email,
            first_name: req.user.firstname,
            last_name: req.user.lastname,
        };

        res.send(JSON.stringify(user));
    });

    app.get('/profile/wardrobe', enforceAuth, (req, res) => {

        var wardrobe = [];

        const query = 'SELECT `article_id` FROM `wardrobe` WHERE `user_id`=?';
        connection.query(query, [req.user.id]).then((rows) => {
            return rows.map((row) => {
                return new Promise(function(resolve, reject) {
                    const id = row.article_id;

                    const q = 'SELECT * FROM `articles` WHERE `id`=?';
                    connection.query(q, [id]).then((articles) => {
                        wardrobe.push(articles[0]);
                        console.log(articles[0]);
                        resolve();

                    });
                });
            });

        }).then((promises) => {
            Promise.all(promises).then(() => {
                res.send(JSON.stringify(wardrobe));
            });
        });

    });

    app.post('/profile/wardrobe', enforceAuth, (req, res) => {
        var article = req.body;
        console.log(article);

        var promise;

        if (!('id' in req.body)) {

            promise = new Promise(function(resolve, reject) {
                const articleInsert = 'INSERT INTO `articles` (`price`, `name`, `maker`, `type`, `color`) VALUES (?, ?, ?, ?, ?)';
                connection.query(articleInsert, [
                    article.price,
                    article.name,
                    article.maker,
                    article.type,
                    article.color
                ]).then(() => {
                    connection.query('SELECT LAST_INSERT_ID();').then((out) => {
                        article.id = out[0]['LAST_INSERT_ID()'];
                        resolve();
                    })
                });
            });

        } else {
            promise = Promise.resolve();
        }

        promise.then(() => {
            const wardrobeInsert = 'INSERT INTO `wardrobe` (`article_id`, `user_id`) VALUES (?, ?)';
            connection.query(wardrobeInsert, [
                article.id,
                req.user.id
            ]).then(() => {
                res.status(200).send('Successful add');
            });
        });
    });

    /** General application routes */
    app.get('/articles', (req, res) => {
        connection.query('SELECT * FROM `articles`').then(function(rows){
            console.log(rows);

            var choosen = [];

            for (var i = 0; i < 20; i++) {
                var row = rows[Math.floor(Math.random()*rows.length)];
                if (row.link == null) {
                    continue;
                }
                choosen.push(row);
            }

            res.send(JSON.stringify(choosen));
        });
    });
};

/** Route middleware to enforce authentication */
function enforceAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.status(403).send('Unauthorized');
    }
}
