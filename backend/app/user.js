const bcrypt = require('bcrypt-nodejs');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user', {
        id: {
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4
        },

        email: DataTypes.STRING,
        password: DataTypes.STRING,
        firstname: DataTypes.STRING,
        lastname: DataTypes.STRING,

    }, {
        classMethods: {
            generateHash: function(password) {
                return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
            },
        },
        instanceMethods: {
            validPassword: function(password) {
                return bcrypt.compareSync(password, this.password);
            }
        },
        getterMethods: {
            someValue: function() {
                return this.someValue;
            }
        },
        setterMethods: {
            someValue: function(value) {
                this.someValue = value;
            }
        }
    });
}
