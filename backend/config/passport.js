const sequelize = new(require('sequelize'))('auth', 'root', null, {
    host: 'localhost',
    dialect: 'mysql'
});

const user = sequelize.import('../app/user.js');
user.sync();

const LocalStrategy = require('passport-local').Strategy;

module.exports = (passport) => {

    /** User serialization */
    passport.serializeUser((user, done) =>
        done(null, user.id)
    );

    /** User deserialization */
    passport.deserializeUser((id, done) => {
        user.findById(id).then((user) =>
            done(null, user)
        ).catch((e) =>
            done(e, false)
        );
    });

    /** User local login strategy */
    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, (req, email, password, done) =>
        user.findOne({
            where: {
                email
            }
        }).then((user) => {
            if (!user) {
                req.res.status(403).send('Unauthorized');

            } else if (!user.validPassword(password)) {
                req.res.status(403).send('Unauthorized');

            } else {
                done(null, user);
            }

        }).catch((e) => {
            console.error(e.stack);
            done(null, false, req.flash('message', `${e.name}  ${e.message}`))
        })
    ));

    /** User local signup strategy */
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, (req, email, password, done) =>
        user.findOne({
            where: {
                email
            }
        }).then((err, existingUser) => {
            if (existingUser) {
                return done(null, false, req.flash('message', 'Email already taken'));
            }

            const newUser = user.build({
                email,
                password: user.generateHash(password),
                firstname: req.body.firstname,
                lastname: req.body.lastname
            });

            return newUser.save()
                .then(() => done(null, newUser))
                .catch((err) => done(null, false, req.flash('message', err)));

        }).catch((e) =>
            done(null, false, req.flash('message', `${e.name}  ${e.message}`))
        )
    ));
}
