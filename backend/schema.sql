DROP TABLE IF EXISTS articles;
CREATE TABLE articles
(
    id INT NOT NULL AUTO_INCREMENT,
    price DECIMAL NOT NULL,
    name NVARCHAR(200) NOT NULL,
    maker NVARCHAR(200) NOT NULL,
    link NVARCHAR(200),
    type SMALLINT NOT NULL,
    color NVARCHAR(200) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS wardrobe;
CREATE TABLE wardrobe
(
    article_id INT NOT NULL,
    user_id CHAR(36) NOT NULL
);
