INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('175.00', '100% Stripe Colorblock Crew Neck Raglan', 'Williams Cashmere', 'https://images-na.ssl-images-amazon.com/images/I/A1eOgvUlBpL._%20UX679_.jpg', 3, 'charcoal');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('34.99', 'Buck Camp Flannels', 'Legendary Whitetails', 'https://images-na.ssl-images-amazon.com/images/I/81oScIO7aEL._SY450_.jpg', 0, 'charcoal');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('34.99', 'Buck Camp Flannels', 'Legendary Whitetails', 'https://images-na.ssl-images-amazon.com/images/I/81gsXllXdTL._SY450_.jpg', 0, 'army');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('34.99', 'Navigator Fleece Button Down', 'Legendary Whitetails', 'https://images-na.ssl-images-amazon.com/images/I/81nQuaL7McL._SL1500_.jpg', 0, 'graphite');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('19.99', 'Authentics Long Sleeve Plaid Fleece Shirt', 'Wrangler', 'https://images-na.ssl-images-amazon.com/images/I/912zG8FYKbL._UX466_.jpg', 0, 'red buffalo plaid');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('24.99', 'Perfect Slim Fit V-Neck Sweater', 'Comfortably Collared ', 'https://images-na.ssl-images-amazon.com/images/I/61SG3Cmc9tL._UX466_.jpg', 3, 'black');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('59.95', 'V-Neck Pullover Sweater', 'Braga', 'https://images-na.ssl-images-amazon.com/images/I/81mFRzyiyRL._UY606_.jpg', 3, 'orange');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('49.95', 'Cashmere Crew Neck V-Neck  Sweater', 'LongMing', 'https://images-na.ssl-images-amazon.com/images/I/71KkAlT0scL._UX569_.jpg', 3, 'red');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('36.99', 'Saltwater Marled Waffle Crew Neck Sweater', 'IZOD', 'https://images-na.ssl-images-amazon.com/images/I/A1d6DL4Pb1L._UX466_.jpg', 3, 'andora');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('19.95', 'Fireside Plaid Flannel Shirt', 'GH Bass', 'https://images-na.ssl-images-amazon.com/images/I/91rlXwMWEeL._UX466_.jpg', 0, 'ensign blue');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('12.99', 'Essential Solid Long Sleeve Shirt', 'IZOD', 'https://images-na.ssl-images-amazon.com/images/I/91NJrbRiWAL._UX466_.jpg', 0, 'blue depths');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('12.99', 'Essential Solid Long Sleeve Shirt', 'IZOD', 'https://images-na.ssl-images-amazon.com/images/I/81Uv4imDDpL._UX466_.jpg', 0, 'white');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('21.65', 'Logger Shirt', 'Wrangler', 'https://images-na.ssl-images-amazon.com/images/I/81tGOHwPaeL._UX466_.jpg', 0, 'burgundy');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('59.99', 'Solid Rolled-Sleeve Linen Shirt', 'Perry Ellis ', 'https://images-na.ssl-images-amazon.com/images/I/A1hVbOJrqRL._UX466_.jpg', 0, 'delt');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('17.99', 'Button Down Long Sleeve Woven Shirt', 'Burnside', 'https://images-na.ssl-images-amazon.com/images/I/A1MkSFlDeoL._UX466_.jpg', 0, 'khaki');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('15.00', 'D2 Straight-Fit Flat-Front Pant', 'Dockers ', 'https://images-na.ssl-images-amazon.com/images/I/813ku8O-2KL._UX466_.jpg', 1, 'British khaki');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('15.00', 'D2 Straight-Fit Flat-Front Pant', 'Dockers ', 'https://images-na.ssl-images-amazon.com/images/I/81Rku5cVCRL._UX466_.jpg', 1, 'navy');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('30.95', 'Five Pocket Canvas Carpenter Pant ', 'Carhartt', 'https://images-na.ssl-images-amazon.com/images/I/814x1RFdovL._UX466_.jpg', 1, 'charcoal');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('58.00', 'Refined Cotton Twill Pant', 'Calvin Klein ', 'https://images-na.ssl-images-amazon.com/images/I/71jVWeCy6aL._UX466_.jpg', 1, 'black');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('58.00', 'Refined Cotton Twill Pant', 'Calvin Klein ', 'https://images-na.ssl-images-amazon.com/images/I/81O2Wl9DK8L._UX466_.jpg', 1, 'convoy');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('35.95', 'Straight Chino Twill Pant', 'Levis', 'https://images-na.ssl-images-amazon.com/images/I/81WaJurgBWL._UX466_.jpg', 1, 'graphite');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('35.95', 'Straight Chino Twill Pant', 'Levis', 'https://images-na.ssl-images-amazon.com/images/I/81iX6oN6xFL._UX466_.jpg', 1, 'true chino');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('35.95', 'Straight Chino Twill Pant', 'Levis', 'https://images-na.ssl-images-amazon.com/images/I/81BbAqpTAoL._UX466_.jpg', 1, 'sundried tomato');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('39.99', '511 Slim-Fit Jean', 'Levis', 'https://images-na.ssl-images-amazon.com/images/I/91riNfbtGvL._UX466_.jpg', 1, 'Sequoia');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('39.99', '511 Slim-Fit Jean', 'Levis', 'https://images-na.ssl-images-amazon.com/images/I/81rBFOfvfLL._UX466_.jpg', 1, 'lodge iron');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('39.99', '511 Slim-Fit Jean', 'Levis', 'https://images-na.ssl-images-amazon.com/images/I/81XtuXCBuaL._UX466_.jpg', 1, 'throttle');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('53.00', 'Bolton Dress Slip-On', 'Bostonian', 'https://images-na.ssl-images-amazon.com/images/I/71cfv6n9R4L._UY575_.jpg', 4, 'black');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('55.97', 'Ipswich Lace-Up Oxford Shoe', 'Bostonian', 'https://images-na.ssl-images-amazon.com/images/I/81fITcf54IL._UX575_.jpg', 4, 'black');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('55.97', 'Ipswich Lace-Up Oxford Shoe', 'Bostonian', 'https://images-na.ssl-images-amazon.com/images/I/81fuKXilG5L._UX575_.jpg', 4, 'brown');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('76.48', 'Moab Waterproof Hiking Shoe', 'Merrell', 'https://images-na.ssl-images-amazon.com/images/I/81oOwcxSbWL._UX575_.jpg', 4, 'olive');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('99.95', 'Moab Rover Moc Slip-On Shoe', 'Merrell', 'https://images-na.ssl-images-amazon.com/images/I/81OEQQE4mVL._UX575_.jpg', 4, 'espresso');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('36.95', 'Atwood Skate Shoe', 'Vans ', 'https://images-na.ssl-images-amazon.com/images/I/81TkprX-nyL._UX575_.jpg', 4, 'black');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('39.99', 'Classic Fit  Estate Mini Plaid Shirt', 'Nautica', 'https://images-na.ssl-images-amazon.com/images/I/A1ykZDhFD1L._UX522_.jpg', 0, 'estate blue');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('17.99', 'Cotton Plaid Long Sleeve  Button Down Shirt', 'Coevals Club ', 'https://images-na.ssl-images-amazon.com/images/I/71y%2BBTHjx4L._UX679_.jpg', 0, 'gray/black');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('71.99', 'Brushed Back Twill Work Shirt', 'Royal Robbins', 'https://images-na.ssl-images-amazon.com/images/I/A1O3PknWbyL._SX466_.jpg', 0, 'loden');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('19.95', 'Long Sleeve Canvas Shirt', 'Wrangler', 'https://images-na.ssl-images-amazon.com/images/I/91OTuxdKGvL._UX466_.jpg', 0, 'anthracite');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('19.95', 'Long Sleeve Canvas Shirt', 'Wrangler', 'https://images-na.ssl-images-amazon.com/images/I/91BkV0NTkIL._UX466_.jpg', 0, 'apple cinnamon');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('19.95', 'Long Sleeve Canvas Shirt', 'Wrangler', 'https://images-na.ssl-images-amazon.com/images/I/91fq%2BHzOCVL._UX466_.jpg', 0, 'new navy');
INSERT INTO articles (price, name, maker, link, type, color) VALUES
    ('44.50', 'Merino Ribbed Crew Neck Sweater', 'Calvin Klein', 'https://images-na.ssl-images-amazon.com/images/I/A1U5FaN1LaL._UX466_.jpg', 3, 'piglio combo');
